import { MongoClient, Db, InsertOneWriteOpResult, Collection } from "mongodb";

export class MongoDB {
    private static instance: Promise<MongoDB> = null;
    private static dbUrl: string;
    private static readonly dbName: string = 'cube-manipulation';
    private static readonly scenesCollectionName: string = 'scenes';

    private dbClient: MongoClient;

    public static gi(): Promise<MongoDB> {
        console.log('MongoDB.gi');
        if (MongoDB.instance === null) {
            console.log(`MongoDB.instance is null`);
            MongoDB.initConnectionUrl();
            MongoDB.instance = MongoClient.connect(
                MongoDB.dbUrl,
                {
                    useNewUrlParser: true,
                    poolSize: 30
                })
                .then((client) => {
                    console.log('new db ready');
                    return new MongoDB(client);
                });
                //TODO: handle connection errors
        }
        return MongoDB.instance;
    }

    private constructor(client: MongoClient) {
        this.dbClient = client;
    }

    public close(): Promise<void> {
        MongoDB.instance = null;
        return  this.dbClient && this.dbClient.close();
    }

    public getLastSceneData(): Promise<any> {
        return this.getScenesCollection().findOne({}, {sort: {'_id': -1}});
    }

    public insertScene(sceneData: any): Promise<InsertOneWriteOpResult> {
        return this.getScenesCollection().insertOne(sceneData);
    }

    private getDB(): Db {
        return this.dbClient.db(MongoDB.dbName);
    }

    private getScenesCollection(): Collection {
        return this.getDB().collection(MongoDB.scenesCollectionName);
    }

    private static initConnectionUrl() {
        if (MongoDB.dbUrl) {
            return;
        }
        let hostname: string = process.env.MONGODB_HOSTNAME || 'localhost';
        let port: string = process.env.MONGODB_PORT || '27017';
        MongoDB.dbUrl = `mongodb://${hostname}:${port}`;
        console.log('DB url: ', MongoDB.dbUrl);
    }
}