import express, { Application } from 'express';
import http from 'http';
import { MongoDB } from './MongDB';
import bodyParser = require('body-parser');

export class Server {
    private host: string;
    private port: number;
    private expressApp: Application;
    private httpServer: http.Server;

    constructor(host: string = 'localhost', port: number = 8080) {
        this.host = host;
        this.port = port;
        this.expressApp = express();
        this.httpServer = new http.Server(this.expressApp);
        this.initializeApi();
    }

    public start() {
        this.httpServer.listen(this.port, this.host, () => {
            console.info(`Server listening at http://${this.host}:${this.port}/`);
        });
    }

    private initializeApi() {
        this.expressApp
            .use(bodyParser.json({limit: '2mb'}))
            .get('/scene', async (req, res) => {
                try {
                    let lastScene = await MongoDB.gi().then((db) => db.getLastSceneData());
                    delete lastScene._id;
                    res.send(lastScene || null);
                } catch (e) {
                    res.sendStatus(500);
                    res.send(e);
                }
            })
            .post('/scene', async (req, res) => {
                try {
                    let sceneData = req.body || null;
                    if (!sceneData) {
                        res.sendStatus(400);
                    }
                    let insertRes = await MongoDB.gi().then((db) => db.insertScene(sceneData));
                    if (insertRes.result.ok) {
                        res.send({id: insertRes.insertedId});
                    } else {
                        res.sendStatus(500);
                    }
                } catch (e) {
                    res.sendStatus(500);
                    res.send(e);
                }
            })
            .get('*', (req, res) => {
                res.send('Hi');
            });
    }

    public stop() {
        this.httpServer.close();
    }
}