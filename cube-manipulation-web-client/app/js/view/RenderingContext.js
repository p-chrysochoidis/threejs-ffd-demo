export default class RenderingContext {
    constructor(scene, camera, renderer, controls) {
        this.scene = scene;
        this.camera = camera;
        this.renderer = renderer;
        this.controls = controls;
    }

    static getDefault(containerElement) {
        const width  = window.innerWidth, height = window.innerHeight;
        const scene = new THREE.Scene();
        const camera = new THREE.PerspectiveCamera(45, width / height, 0.01, 1000);
        const renderer = new THREE.WebGLRenderer();
        const controls = new THREE.OrbitControls(camera, renderer.domElement);

        camera.position.z = 30;
        renderer.setSize(width, height);
        renderer.setClearColor(0xf0f0f0, 1);
        scene.add(new THREE.AmbientLight(0xffffff));

        const light = new THREE.DirectionalLight(0xffffff, 1);

        light.position.set(15,15,15);
        scene.add(light);

        //adding random geometries//
        var geometry = new THREE.BoxGeometry( 4, 4, 4, 1, 1, 1 );
        for ( var i = 0; i < 100; i ++ ) {
					// var object = new THREE.Mesh( geometry, new THREE.MeshNormalMaterial({ wireframe: true }) );
					var object = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff, wireframe: false } ) );

					object.position.x = Math.random() * 500 - 100;
					object.position.y = Math.random() * 120 - 60;
					object.position.z = Math.random() * 80 - 40;
					object.rotation.x = Math.random() * 2 * Math.PI;
					object.rotation.y = Math.random() * 2 * Math.PI;
					object.rotation.z = Math.random() * 2 * Math.PI;
					object.scale.x = Math.random() * 2 + 1;
					object.scale.y = Math.random() * 2 + 1;
					object.scale.z = Math.random() * 2 + 1;

					object.castShadow = true;
					object.receiveShadow = true;
					scene.add( object );

				 }


        containerElement.appendChild(renderer.domElement);

        return new RenderingContext(scene, camera, renderer, controls);



    }
}
